package br.com.luis.core.data

import br.com.luis.core.domain.GistError

sealed class State<out T> {
    data class Success<T>(val data: T) : State<T>()
    data class Failure(val error: GistError) : State<Nothing>()
}
