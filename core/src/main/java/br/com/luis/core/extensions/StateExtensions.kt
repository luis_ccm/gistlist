package br.com.luis.core.extensions

import br.com.luis.core.data.State
import br.com.luis.core.domain.GistError

inline fun <T> State<T>.onSuccess(action: (value: T) -> Unit): State<T> {
    if (this is State.Success) {
        action.invoke(data)
    }
    return this
}

inline fun <T> State<T>.onFailure(action: (value: GistError) -> Unit): State<T> {
    if (this is State.Failure) {
        action.invoke(error)
    }
    return this
}