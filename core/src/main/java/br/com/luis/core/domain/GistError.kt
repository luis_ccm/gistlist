package br.com.luis.core.domain

import java.lang.Exception

sealed class GistError(val exception: Exception) {
    open class BusinessError(exception: Exception) : GistError(exception)
    open class Network(exception: Exception) : GistError(exception)
}
