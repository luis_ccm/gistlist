package br.com.luis.core.extensions

import androidx.fragment.app.Fragment
import androidx.lifecycle.LiveData

inline fun <reified T> Fragment.observe(
    liveData: LiveData<T>,
    crossinline execution: (T) -> Unit
) {
    liveData.observe(viewLifecycleOwner,  { execution(it) })
}
