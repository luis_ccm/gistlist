package br.com.luis.core.util


import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

inline fun <reified T> fromJson(text: String): T {
    return Gson().fromJson(text, object : TypeToken<T>() {}.type)
}