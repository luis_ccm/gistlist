package br.com.luis.core.extensions

import android.content.ActivityNotFoundException
import android.content.Intent
import android.net.Uri
import androidx.fragment.app.Fragment

fun Fragment.openUrl(site: String) {
    try {
        val openURL = Intent(Intent.ACTION_VIEW)
        openURL.addCategory(Intent.CATEGORY_BROWSABLE)
        openURL.data = Uri.parse(site)
        startActivity(openURL)
    } catch (e: ActivityNotFoundException) {
    }
}