plugins {
    id(AppConfiguration.Plugins.androidLibrary)
    kotlin(AppConfiguration.Plugins.kotlinAndroid)
    kotlin(AppConfiguration.Plugins.kotlinAndroidExtensions)
}

android {
    compileSdkVersion(AppConfiguration.compileSdkVersion)
    buildToolsVersion(AppConfiguration.buildToolsVersion)

    defaultConfig {
        minSdkVersion(AppConfiguration.minSdkVersion)
        targetSdkVersion(AppConfiguration.targetSdkVersion)
        versionCode = AppConfiguration.versionCode
        versionName = AppConfiguration.versionName
        vectorDrawables.useSupportLibrary = true
        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    }

    buildTypes {
        val releaseType = AppConfiguration.BuildType.Release
        getByName(releaseType.name) {
            isMinifyEnabled = releaseType.isMinifyEnabled
        }

        val debugType = AppConfiguration.BuildType.Debug
        getByName(debugType.name) {
            isMinifyEnabled = debugType.isMinifyEnabled
            //isTestCoverageEnabled = true
        }
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
    kotlinOptions {
        jvmTarget = "1.8"
    }

    testOptions {
        unitTests.isReturnDefaultValues = true
        animationsDisabled = true
        unitTests.isIncludeAndroidResources = true
    }
}

dependencies {
    AppConfiguration.libsImplementations.forEach { lib ->
        implementation(lib)
    }

    AppConfiguration.testImplementationLibs.forEach { lib ->
        testImplementation(lib)
    }

    implementation("com.squareup.retrofit2:converter-gson:2.9.0")
}