plugins {
    id(AppConfiguration.Plugins.androidApplication)
    kotlin(AppConfiguration.Plugins.kotlinAndroid)
    kotlin(AppConfiguration.Plugins.kotlinAndroidExtensions)
    id(AppConfiguration.Plugins.kotlinAndroidPlugin)
    id(AppConfiguration.Plugins.safeArgs)
}

apply {
    from("../detekt.gradle")
    from("../jacoco-config.gradle")
}

android {
    compileSdkVersion(AppConfiguration.compileSdkVersion)
    buildToolsVersion(AppConfiguration.buildToolsVersion)

    packagingOptions {
        exclude("META-INF/LICENSE*")
        exclude("META-INF/AL2.0")
        exclude("META-INF/LGPL2.1")
    }
    defaultConfig {
        applicationId = AppConfiguration.applicationId
        minSdkVersion(AppConfiguration.minSdkVersion)
        targetSdkVersion(AppConfiguration.targetSdkVersion)
        versionCode = AppConfiguration.versionCode
        versionName = AppConfiguration.versionName
        vectorDrawables.useSupportLibrary = true
        testInstrumentationRunner = "br.com.luis.gistlist.EspressoRunner"
    }

    buildTypes {
        val releaseType = AppConfiguration.BuildType.Release
        getByName(releaseType.name) {
            isMinifyEnabled = releaseType.isMinifyEnabled
            isShrinkResources = releaseType.isShrinkResources
            proguardFiles(getDefaultProguardFile(releaseType.defaultProguardFile), releaseType.defaultProguardRules)
        }

        val debugType = AppConfiguration.BuildType.Debug
        getByName(debugType.name) {
            isMinifyEnabled = debugType.isMinifyEnabled
            isShrinkResources = debugType.isShrinkResources
            //isTestCoverageEnabled = true
            applicationIdSuffix = debugType.suffix

        }
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
    kotlinOptions {
        jvmTarget = "1.8"
    }

    testOptions {
        unitTests.isReturnDefaultValues = true
        animationsDisabled = true
        unitTests.isIncludeAndroidResources = true
    }

    buildFeatures {
        viewBinding = true
    }

    lintOptions {
        isCheckReleaseBuilds = false
        isQuiet = true
        isAbortOnError = false
        disable("MissingTranslation")
    }
}


dependencies {
    implementation(project(":core"))

    AppConfiguration.libsImplementations.forEach { lib ->
        implementation(lib)
    }

    AppConfiguration.testImplementationLibs.forEach { lib ->
        testImplementation(lib)
    }

    AppConfiguration.androidTestImplementationLibs.forEach { lib ->
        androidTestImplementation(lib)
    }

    AppConfiguration.appImplementations.forEach { lib ->
        implementation(lib)
    }

    AppConfiguration.debugImplementations.forEach { lib ->
        debugImplementation(lib)
    }
}