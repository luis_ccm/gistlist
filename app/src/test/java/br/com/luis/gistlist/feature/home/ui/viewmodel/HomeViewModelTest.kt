package br.com.luis.gistlist.feature.home.ui.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import br.com.luis.TestUtil
import br.com.luis.core.data.State
import br.com.luis.core.util.fromJson
import br.com.luis.gistlist.CoroutinesTestRule
import br.com.luis.gistlist.feature.home.data.mapper.mapToDomain
import br.com.luis.gistlist.feature.home.data.model.GistModelPayload
import br.com.luis.gistlist.feature.home.domain.model.GistResponseModel
import br.com.luis.gistlist.feature.home.domain.repository.GistsRepository
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.Assert
import org.junit.Rule
import org.junit.Test

@ExperimentalCoroutinesApi
class HomeViewModelTest {

    private val repositoryMock: GistsRepository = mockk()
    private val viewModel = HomeViewModel(repositoryMock)
    private val  observer: Observer<List<GistResponseModel>> = mockk(relaxed = true)

    @get:Rule
    var coroutinesTestRule = CoroutinesTestRule()

    @Rule @JvmField
    val instantTaskExecutorRule = InstantTaskExecutorRule()


    @Test
    fun `GIVEN a call on getGistList method  getGistList on GistsRepository SHOULD be invoked once`() {
        val gistList = fromJson<List<GistModelPayload>>(TestUtil.jsonText).map { it.mapToDomain() }
        coEvery { repositoryMock.getPublicGists() } returns State.Success(gistList)
        viewModel.getGistList()

        coVerify(exactly = 1) {
            repositoryMock.getPublicGists()
        }
    }

    @Test
    fun `GIVEN a valid call on getGistList method on GistsRepository gistListLiveData SHOULD be filled with that value`() {
        val gistList = fromJson<List<GistModelPayload>>(TestUtil.jsonText).map { it.mapToDomain() }
        coEvery { repositoryMock.getPublicGists() } returns State.Success(gistList)
        viewModel.gistListLiveData.observeForever(observer)

        viewModel.getGistList()

        Assert.assertEquals(viewModel.loadingLiveData.value, Unit)
        Assert.assertEquals(viewModel.gistListLiveData.value, gistList)
        Assert.assertNull(viewModel.errorLiveData.value)
    }

    @Test
    fun `GIVEN an invalid call on getGistList method on GistsRepository errorLiveData SHOULD be filled with that value`() {
        val expectedResult = State.Failure(mockk())
        coEvery { repositoryMock.getPublicGists() } returns expectedResult
        viewModel.gistListLiveData.observeForever(observer)

        viewModel.getGistList()

        Assert.assertEquals(viewModel.loadingLiveData.value, Unit)
        Assert.assertNull(viewModel.gistListLiveData.value)
        Assert.assertEquals(viewModel.errorLiveData.value, expectedResult.error)
    }
}