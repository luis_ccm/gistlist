package br.com.luis.gistlist.feature.home.data.mapper

import br.com.luis.gistlist.feature.home.data.model.GistModelPayload
import br.com.luis.gistlist.feature.home.data.model.GistOwnerPayload
import br.com.luis.gistlist.feature.home.domain.model.GistOwnerResponseModel
import br.com.luis.gistlist.feature.home.domain.model.GistResponseModel
import org.junit.Assert
import org.junit.Test
import java.util.Calendar

class GistsExtensionsTest {

    @Test
    fun `GIVEN an GistOwnerPayload mapToDomain method SHOULD return a valid GistOwnerResponseModel`() {
        val expectedResult = GistOwnerResponseModel(
            id = 1,
            name = "Luis Henrique",
            url = "https://pt.stackoverflow.com/",
            avatar = "https://www.google.com.br/"
        )

        val payload = GistOwnerPayload(
            id = 1,
            url = "https://pt.stackoverflow.com/",
            avatarUrl = "https://www.google.com.br/",
            login = "Luis Henrique"
        )

        Assert.assertEquals(expectedResult, payload.mapToDomain())
    }

    @Test
    fun `GIVEN an GistModelPayload mapToDomain method SHOULD return a valid GistResponseModel`() {
        val owner = GistOwnerResponseModel(
            id = 1,
            name = "Luis Henrique",
            url = "https://pt.stackoverflow.com/",
            avatar = "https://www.google.com.br/"
        )
        val ownerPayload = GistOwnerPayload(
            id = 1,
            url = "https://pt.stackoverflow.com/",
            avatarUrl = "https://www.google.com.br/",
            login = "Luis Henrique"
        )

        val typeMap = mapOf<String, Any>(pair = Pair("type", "application/x-ruby"))
        val testDate = Calendar.getInstance()
        testDate.set(2020, 9, 10, 9, 0, 0)

        val payload =
            GistModelPayload(
                id = "100",
                owner = ownerPayload,
                description = "Teste Unitário",
                files = mapOf<String, Any>(Pair("teste", typeMap)),
                createdAt = testDate.time
            )

        val expectedResult = GistResponseModel(
            id = "100",
            description = "Teste Unitário",
            owner = owner,
            type = "application/x-ruby",
            createdAt = "Sat Oct 10 09:00:00 BRT 2020"
        )

        Assert.assertEquals(expectedResult, payload.mapToDomain())
    }

    @Test
    fun `GIVEN an GistModelPayload with invalid files mapToDomain method SHOULD return a valid GistResponseModel`() {
        val owner = GistOwnerResponseModel(
            id = 1,
            name = "Luis Henrique",
            url = "https://pt.stackoverflow.com/",
            avatar = "https://www.google.com.br/"
        )
        val ownerPayload = GistOwnerPayload(
            id = 1,
            url = "https://pt.stackoverflow.com/",
            avatarUrl = "https://www.google.com.br/",
            login = "Luis Henrique"
        )

        val testDate = Calendar.getInstance()
        testDate.set(2020, 9, 10, 9, 0, 0)

        val payload =
            GistModelPayload(
                id = "100",
                owner = ownerPayload,
                description = "Teste Unitário",
                files = mapOf(),
                createdAt = testDate.time
            )

        val expectedResult = GistResponseModel(
            id = "100",
            description = "Teste Unitário",
            owner = owner,
            type = "",
            createdAt = "Sat Oct 10 09:00:00 BRT 2020"
        )

        Assert.assertEquals(expectedResult, payload.mapToDomain())
    }

    @Test
    fun `GIVEN an GistModelPayload with nullable description mapToDomain method SHOULD return a valid GistResponseModel`() {
        val owner = GistOwnerResponseModel(
            id = 1,
            name = "Luis Henrique",
            url = "https://pt.stackoverflow.com/",
            avatar = "https://www.google.com.br/"
        )
        val ownerPayload = GistOwnerPayload(
            id = 1,
            url = "https://pt.stackoverflow.com/",
            avatarUrl = "https://www.google.com.br/",
            login = "Luis Henrique"
        )

        val typeMap = mapOf<String, Any>(pair = Pair("type", "application/x-ruby"))
        val testDate = Calendar.getInstance()
        testDate.set(2020, 9, 10, 9, 0, 0)

        val payload =
            GistModelPayload(
                id = "100",
                owner = ownerPayload,
                description = null,
                files = mapOf<String, Any>(Pair("teste", typeMap)),
                createdAt = testDate.time
            )

        val expectedResult = GistResponseModel(
            id = "100",
            description = "",
            owner = owner,
            type = "application/x-ruby",
            createdAt = "Sat Oct 10 09:00:00 BRT 2020"
        )

        Assert.assertEquals(expectedResult, payload.mapToDomain())
    }
    @Test
    fun `GIVEN an GistModelPayload with nullable type mapToDomain method SHOULD return a valid GistResponseModel`() {
        val owner = GistOwnerResponseModel(
            id = 1,
            name = "Luis Henrique",
            url = "https://pt.stackoverflow.com/",
            avatar = "https://www.google.com.br/"
        )
        val ownerPayload = GistOwnerPayload(
            id = 1,
            url = "https://pt.stackoverflow.com/",
            avatarUrl = "https://www.google.com.br/",
            login = "Luis Henrique"
        )

        val typeMap = mapOf<String, Any?>(pair = Pair("type", null))
        val testDate = Calendar.getInstance()
        testDate.set(2020, 9, 10, 9, 0, 0)

        val payload =
            GistModelPayload(
                id = "100",
                owner = ownerPayload,
                description = null,
                files = mapOf<String, Any>(Pair("teste", typeMap)),
                createdAt = testDate.time
            )

        val expectedResult = GistResponseModel(
            id = "100",
            description = "",
            owner = owner,
            type = "",
            createdAt = "Sat Oct 10 09:00:00 BRT 2020"
        )

        Assert.assertEquals(expectedResult, payload.mapToDomain())
    }
}
