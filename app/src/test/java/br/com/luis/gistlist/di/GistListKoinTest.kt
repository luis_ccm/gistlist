package br.com.luis.gistlist.di

import android.content.Context
import io.mockk.mockk
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.loadKoinModules
import org.koin.core.context.startKoin
import org.koin.core.context.stopKoin
import org.koin.test.KoinTest
import org.koin.test.check.checkModules


class GistListKoinTest: KoinTest {

    private val context : Context = mockk()

    @Before
    fun setup() {
        startKoin {
            androidContext(context)
            loadKoinModules(GistModules.modules)
        }
    }

    @Test
    fun checkModulesTest() = getKoin().checkModules()

    @After
    fun tearDown() = stopKoin()
}