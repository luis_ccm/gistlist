package br.com.luis.gistlist.feature.home.domain.repository

import br.com.luis.TestUtil
import br.com.luis.core.coroutine.CoroutineProvider
import br.com.luis.core.data.State
import br.com.luis.core.util.fromJson
import br.com.luis.gistlist.feature.home.data.mapper.mapToDomain
import br.com.luis.gistlist.feature.home.data.model.GistModelPayload
import br.com.luis.gistlist.feature.home.data.service.GistsService
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert
import org.junit.Test

@ExperimentalCoroutinesApi
class GistsRepositoryImplTest {

    private val serviceMock: GistsService = mockk()
    private val testCoroutineDispatcher = TestCoroutineDispatcher()
    private val coroutineProvider = CoroutineProvider(
        mainDispatcher = testCoroutineDispatcher,
        ioDispatcher = testCoroutineDispatcher
    )
    private val repository = GistsRepositoryImpl(service = serviceMock, coroutineProvider = coroutineProvider)

    @Test
    fun `GIVEN a valid CALL getPublicGists getPublicGists method on GistsService SHOULD be invoked once`() {
        testCoroutineDispatcher.runBlockingTest {
            val gistList = fromJson<List<GistModelPayload>>(TestUtil.jsonText)
            coEvery { serviceMock.getPublicGists(50, 1) } returns gistList

            repository.getPublicGists()

            coVerify(exactly = 1) {
                serviceMock.getPublicGists(50, 1)
            }
        }
    }

    @Test
    fun `GIVEN a valid CALL getPublicGists method SHOULD return a valid Response`() {
        testCoroutineDispatcher.runBlockingTest {
            val gistList = fromJson<List<GistModelPayload>>(TestUtil.jsonText)
            coEvery { serviceMock.getPublicGists(50, 1) } returns gistList
            val expectedResult = State.Success(gistList.map { it.mapToDomain() })

            val result = repository.getPublicGists()

            Assert.assertTrue(result is State.Success)
            Assert.assertEquals(expectedResult, result)
        }
    }

    @Test
    fun `GIVEN an invalid CALL getPublicGists method SHOULD return Failure`() {
        testCoroutineDispatcher.runBlockingTest {
            coEvery { serviceMock.getPublicGists(50, 1) } throws Exception()

            val result = repository.getPublicGists()

            Assert.assertTrue(result is State.Failure)
        }
    }
}