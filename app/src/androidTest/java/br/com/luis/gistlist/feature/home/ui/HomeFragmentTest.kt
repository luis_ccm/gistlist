package br.com.luis.gistlist.feature.home.ui

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.navigation.testing.TestNavHostController
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.internal.runner.junit4.statement.UiThreadStatement.runOnUiThread
import br.com.luis.core.data.State
import br.com.luis.core.util.fromJson
import br.com.luis.gistlist.R
import br.com.luis.gistlist.TestUtil
import br.com.luis.gistlist.feature.home.data.mapper.mapToDomain
import br.com.luis.gistlist.feature.home.data.model.GistModelPayload
import br.com.luis.gistlist.feature.home.domain.repository.GistsRepository
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.koin.core.context.loadKoinModules
import org.koin.core.context.unloadKoinModules
import org.koin.dsl.module

@ExperimentalCoroutinesApi
@RunWith(AndroidJUnit4::class)
class HomeFragmentTest {

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    private val navController by lazy { TestNavHostController(ApplicationProvider.getApplicationContext()) }

    private val repository: GistsRepository = mockk()

    private val testModule = module(override = true) {
        single { repository }
    }


    @Before
    fun setup() {
        loadKoinModules(testModule)

        runOnUiThread {
            navController.setGraph(R.navigation.nav_graph)
            navController.setCurrentDestination(R.id.HomeFragment)
        }

    }

    @After
    fun unloadModules() {
        unloadKoinModules(testModule)
    }

    @Test
    fun when_open_screen_only_loading_should_be_visible() {
        coEvery { repository.getPublicGists() } returns mockk()
        onLaunch(navController) {

        } checkIf {
            loadingIsDisplayed()
            recyclerViewIsNotDisplayed()
            errorViewIsNotDisplayed()
        }
    }

    @Test
    fun when_repository_returns_error_only_error_container_should_be_visible() {
        coEvery { repository.getPublicGists() } returns State.Failure(mockk())
        onLaunch(navController) {
            waitErrorViewAppear()
        } checkIf {
            loadingIsNotDisplayed()
            recyclerViewIsNotDisplayed()
            errorViewIsDisplayed()
        }
    }

    @Test
    fun when_repository_returns_success_list_container_should_be_visible() {
        val gistList = fromJson<List<GistModelPayload>>(TestUtil.jsonText).map { it.mapToDomain() }
        coEvery { repository.getPublicGists() } returns State.Success(gistList)
        onLaunch(navController) {
            waitListViewAppear()
        } checkIf {
            loadingIsNotDisplayed()
            recyclerViewIsDisplayed()
            errorViewIsNotDisplayed()
        }
    }

    @Test
    fun when_click_on_list_should_navigate_to_detail() {
        val gistList = fromJson<List<GistModelPayload>>(TestUtil.jsonText).map { it.mapToDomain() }
        coEvery { repository.getPublicGists() } returns State.Success(gistList)
        onLaunch(navController) {
            waitListViewAppear()
            clickFirstItem()
        } checkIf {
           navigateToDetail(navController)
        }
    }
}