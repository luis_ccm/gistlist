package br.com.luis.gistlist.feature.home.ui

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.navigation.testing.TestNavHostController
import androidx.test.core.app.ApplicationProvider
import androidx.test.espresso.intent.Intents
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.internal.runner.junit4.statement.UiThreadStatement
import br.com.luis.gistlist.R
import br.com.luis.gistlist.feature.home.domain.model.GistOwnerResponseModel
import br.com.luis.gistlist.feature.home.domain.model.GistResponseModel
import br.com.luis.gistlist.feature.home.ui.view.GistDetailFragmentArgs
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.koin.core.context.loadKoinModules

@ExperimentalCoroutinesApi
@RunWith(AndroidJUnit4::class)
class GistDetailFragmentTest {


    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    private val navController by lazy { TestNavHostController(ApplicationProvider.getApplicationContext()) }
    private val mockGist = GistResponseModel(
        id = "100", type = "kotlin", createdAt = "10/10/2021", description = "Código Kotlin",
        owner = GistOwnerResponseModel(
            id = 1,
            name = "Luis Henrique",
            avatar = "https://www.google.com.br",
            url = "https://www.google.com.br"
        )
    )
    private val argsBundle = GistDetailFragmentArgs(mockGist).toBundle()

    @Before
    fun setup() {
        UiThreadStatement.runOnUiThread {
            navController.setGraph(R.navigation.nav_graph)
            navController.setCurrentDestination(R.id.GistDetailFragment)
        }

        Intents.init()
    }

    @After
    fun tearDown() {
        Intents.release()
    }

    @Test
    fun when_open_screen_should_show_gist_detail() {
        onLaunch(navController, argsBundle) {

        } checkIf {
            imageIsDisplayed()
            nameIsDisplayed()
            creationDateIsDisplayed()
            typeIsDisplayed()
            descriptionIsDisplayed()
            urlIsDisplayed()
        }
    }

    @Test
    fun when_open_click_on_url_should_open_link() {
        onLaunch(navController, argsBundle) {
            clickOnUrl()
        } checkIf {
           urlIntentIsOpen()
        }
    }


}