package br.com.luis.gistlist.feature.home.ui

import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.navigation.NavController
import androidx.navigation.Navigation
import br.com.luis.gistlist.BaseRobot
import br.com.luis.gistlist.R
import br.com.luis.gistlist.feature.home.ui.view.HomeFragment
import org.junit.Assert

class HomeFragmentRobot : BaseRobot<HomeFragment>() {

    fun waitErrorViewAppear() {
        waitUntilViewIsDisplayed(R.id.gistError)
    }

    fun waitListViewAppear() {
        waitUntilViewIsDisplayed(R.id.gistList)
    }

    fun clickFirstItem() {
        clickAtRecyclerViewPosition(R.id.gistList, 0)
    }

    infix fun checkIf(func: HomeFragmentResult.() -> Unit) =
        HomeFragmentResult().apply(func)
}

class HomeFragmentResult : BaseRobot<HomeFragment>() {

    fun loadingIsDisplayed() {
        checkIfViewIsDisplayed(R.id.gistLoading)
    }

    fun loadingIsNotDisplayed() {
        checkIfViewIsNotDisplayed(R.id.gistLoading)
    }

    fun recyclerViewIsNotDisplayed() {
        checkIfViewIsNotDisplayed(R.id.gistList)
    }

    fun recyclerViewIsDisplayed() {
        checkIfViewIsDisplayed(R.id.gistList)
    }

    fun errorViewIsNotDisplayed() {
        checkIfViewIsNotDisplayed(R.id.gistError)
    }

    fun errorViewIsDisplayed() {
        checkIfViewIsDisplayed(R.id.gistError)
        checkIfViewIsDisplayed(R.id.homeErrorTitle)
        checkIfViewIsDisplayed(R.id.homeErrorAnimation)
        assertText(
            R.id.homeErrorTitle,
            "Não foi possível recuperar a lista.\n Tente novamente mais tarde."
        )
    }

    fun navigateToDetail(navController: NavController) {
        Assert.assertEquals(navController.currentDestination?.id, R.id.GistDetailFragment)
    }
}

internal fun onLaunch(
    navController: NavController,
    event: HomeFragmentRobot.() -> Unit = {}
): HomeFragmentRobot {
    launchFragmentInContainer {
        HomeFragment().also { fragment ->
            fragment.viewLifecycleOwnerLiveData.observeForever {
                if (it != null)
                    Navigation.setViewNavController(fragment.requireView(), navController)
            }
        }
    }
    return HomeFragmentRobot().apply(event)
}