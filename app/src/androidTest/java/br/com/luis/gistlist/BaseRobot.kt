package br.com.luis.gistlist

import android.view.View
import androidx.annotation.IdRes
import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.Espresso
import androidx.test.espresso.IdlingRegistry
import androidx.test.espresso.IdlingResource
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.BoundedMatcher
import androidx.test.espresso.matcher.ViewMatchers
import org.hamcrest.Description
import org.hamcrest.Matcher
import org.hamcrest.Matchers
import org.hamcrest.core.IsNot

open class BaseRobot<T> {

    fun checkIfViewIsDisplayed(@IdRes viewId: Int): BaseRobot<T> {
        Espresso.onView(Matchers.allOf(ViewMatchers.withId(viewId)))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
        return this
    }

    fun checkIfViewIsNotDisplayed(@IdRes viewId: Int): BaseRobot<T> {
        Espresso.onView(Matchers.allOf(ViewMatchers.withId(viewId)))
            .check(ViewAssertions.matches(IsNot.not(ViewMatchers.isDisplayed())))
        return this
    }

    fun assertText(@IdRes viewId: Int, text: String): BaseRobot<T> {
        Espresso.onView(ViewMatchers.withId(viewId))
            .check(ViewAssertions.matches(ViewMatchers.withText(text)))
        return this
    }

    fun waitUntilViewIsDisplayed(@IdRes viewId: Int): BaseRobot<T> {
        waitUntilViewIsDisplayed(ViewMatchers.withId(viewId))
        return this
    }


    fun clickAtRecyclerViewPosition(@IdRes viewId: Int, position: Int): BaseRobot<T> {
        Espresso.onView(ViewMatchers.withId(viewId)).perform(
            RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(
                position,
                ViewActions.click()
            )
        )
        return this
    }

    fun clickOnView(@IdRes viewId: Int): BaseRobot<T> {
        Espresso.onView(Matchers.allOf(ViewMatchers.withId(viewId))).perform(ViewActions.click())
        return this
    }

    private fun atPosition(position: Int, itemMatcher: Matcher<View?>): Matcher<View?> {
        return object : BoundedMatcher<View?, RecyclerView>(RecyclerView::class.java) {
            override fun describeTo(description: Description) {
                description.appendText("has item at position $position: ")
                itemMatcher.describeTo(description)
            }

            override fun matchesSafely(view: RecyclerView): Boolean {
                val viewHolder = view.findViewHolderForAdapterPosition(position) ?: return false
                return itemMatcher.matches(viewHolder.itemView)
            }
        }
    }
}
