package br.com.luis.gistlist.feature.home.ui

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.test.espresso.intent.Intents.intended
import androidx.test.espresso.intent.matcher.IntentMatchers.hasAction
import androidx.test.espresso.intent.matcher.IntentMatchers.hasCategories
import androidx.test.espresso.intent.matcher.IntentMatchers.hasData
import br.com.luis.gistlist.BaseRobot
import br.com.luis.gistlist.R
import br.com.luis.gistlist.feature.home.ui.view.GistDetailFragment
import br.com.luis.gistlist.feature.home.ui.view.HomeFragment
import org.hamcrest.Matchers


class GistDetailFragmentRobot : BaseRobot<HomeFragment>() {

    fun clickOnUrl() {
        clickOnView(R.id.gistDetailUrl)
    }

    infix fun checkIf(func: GistDetailFragmentResult.() -> Unit) =
        GistDetailFragmentResult().apply(func)
}

class GistDetailFragmentResult : BaseRobot<HomeFragment>() {

    fun imageIsDisplayed() {
        checkIfViewIsDisplayed(R.id.gistDetailPhoto)
    }

    fun nameIsDisplayed() {
        checkIfViewIsDisplayed(R.id.gistDetailName)
        assertText(R.id.gistDetailName, "Luis Henrique")
    }

    fun typeIsDisplayed() {
        checkIfViewIsDisplayed(R.id.gistDetailItemType)
        assertText(R.id.gistDetailItemType, "kotlin")
    }
    fun descriptionIsDisplayed() {
        checkIfViewIsDisplayed(R.id.gistDetailDescription)
        assertText(R.id.gistDetailDescription, "Código Kotlin")
    }

    fun creationDateIsDisplayed() {
        checkIfViewIsDisplayed(R.id.gistDetailDate)
        assertText(R.id.gistDetailDate, "Data de criação: 10/10/2021")
    }


    fun urlIsDisplayed() {
        checkIfViewIsDisplayed(R.id.gistDetailUrl)
        assertText(R.id.gistDetailUrl, "Link: https://www.google.com.br")
    }

    fun urlIntentIsOpen() {
        intended(
            Matchers.allOf(
                hasCategories(mutableSetOf(Intent.CATEGORY_BROWSABLE)),
                hasAction(Intent.ACTION_VIEW),
                hasData(Uri.parse("https://www.google.com.br"))
            )
        )
    }

}

internal fun onLaunch(
    navController: NavController,
    args: Bundle,
    event: GistDetailFragmentRobot.() -> Unit = {}
): GistDetailFragmentRobot {
    launchFragmentInContainer(args) {
        GistDetailFragment().also { fragment ->
            fragment.viewLifecycleOwnerLiveData.observeForever {
                if (it != null)
                    Navigation.setViewNavController(fragment.requireView(), navController)
            }
        }
    }
    return GistDetailFragmentRobot().apply(event)
}