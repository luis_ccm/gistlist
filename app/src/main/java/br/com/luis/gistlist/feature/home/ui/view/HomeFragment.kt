package br.com.luis.gistlist.feature.home.ui.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import br.com.luis.core.extensions.observe
import br.com.luis.core.extensions.viewBinding
import br.com.luis.gistlist.databinding.FragmentHomeBinding
import br.com.luis.gistlist.feature.home.domain.model.GistResponseModel
import br.com.luis.gistlist.feature.home.ui.adapter.GistListAdapter
import br.com.luis.gistlist.feature.home.ui.viewmodel.HomeViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class HomeFragment : Fragment() {

    private var binding by viewBinding<FragmentHomeBinding>()
    private val viewModel: HomeViewModel by viewModel()
    private val adapter = GistListAdapter(lifecycleOwner = lifecycle,
        onItemClick = {
            navigateToDetailScreen(it)
        }, onFavoriteClick = {

        })

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentHomeBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        configureView()
        observeResults()
        viewModel.getGistList()
    }

    private fun configureView() {
        binding.gistList.adapter = adapter
    }

    private fun observeResults() {

        observe(viewModel.loadingLiveData) {
            showLoading()
        }

        observe(viewModel.errorLiveData) {
            showError()
        }

        observe(viewModel.gistListLiveData) {
            showItems(it)
        }
    }

    private fun showItems(items: List<GistResponseModel>) {
        binding.gistError.isVisible = false
        binding.gistList.isVisible = true
        binding.gistLoading.isVisible = false
        adapter.submitList(items)
    }


    private fun showLoading() {
        binding.gistError.isVisible = false
        binding.gistList.isVisible = false
        binding.gistLoading.isVisible = true
    }

    private fun showError() {
        binding.gistError.isVisible = true
        binding.gistList.isVisible = false
        binding.gistLoading.isVisible = false
    }

    private fun navigateToDetailScreen(model: GistResponseModel) {
        findNavController().navigate(
            HomeFragmentDirections.actionHomeFragmentToGistDetailFragment(
                model
            )
        )
    }
}