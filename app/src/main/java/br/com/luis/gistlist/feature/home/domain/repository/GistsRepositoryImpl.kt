package br.com.luis.gistlist.feature.home.domain.repository

import br.com.luis.core.coroutine.CoroutineProvider
import br.com.luis.core.data.State
import br.com.luis.core.domain.GistError
import br.com.luis.gistlist.feature.home.data.mapper.mapToDomain
import br.com.luis.gistlist.feature.home.data.service.GistsService
import br.com.luis.gistlist.feature.home.domain.model.GistResponseModel
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.withContext

class GistsRepositoryImpl(
    private val service: GistsService,
    private val coroutineProvider: CoroutineProvider
) : GistsRepository {
    companion object {
        private const val pageSize = 50
        private const val page = 1
    }

    override suspend fun getPublicGists(): State<List<GistResponseModel>> {
        return coroutineScope {
            withContext(coroutineProvider.ioDispatcher) {
                try {
                    State.Success(
                        service.getPublicGists(page = page, maxItems = pageSize)
                            .map { it.mapToDomain() })
                } catch (exception: Exception) {
                    State.Failure(GistError.BusinessError(exception))
                }
            }
        }
    }
}