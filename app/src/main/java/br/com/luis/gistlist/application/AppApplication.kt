package br.com.luis.gistlist.application

import android.app.Application
import br.com.luis.gistlist.di.GistModules
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.loadKoinModules
import org.koin.core.context.startKoin

class AppApplication: Application()  {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(applicationContext)
            loadKoinModules(GistModules.modules)
        }
    }
}