package br.com.luis.gistlist.feature.home.data.service

import br.com.luis.gistlist.feature.home.data.model.GistModelPayload
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Query

interface GistsService {
    @Headers("Content-Type: application/json")
    @GET("public")
    suspend fun getPublicGists(
        @Query("per_page") maxItems: Int,
        @Query("page") page: Int
    ): List<GistModelPayload>
}
