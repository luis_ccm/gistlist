package br.com.luis.gistlist.feature.home.domain.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class GistOwnerResponseModel(
    val id: Int,
    val name: String,
    val avatar: String,
    val url: String
) : Parcelable
