package br.com.luis.gistlist.feature.home.domain.repository

import br.com.luis.core.data.State
import br.com.luis.gistlist.feature.home.domain.model.GistResponseModel

interface GistsRepository {
    suspend fun getPublicGists(): State<List<GistResponseModel>>
}
