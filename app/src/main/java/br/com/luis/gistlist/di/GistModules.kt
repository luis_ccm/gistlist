package br.com.luis.gistlist.di

import br.com.luis.gistlist.feature.home.di.HomeModule

object GistModules{
    val modules = listOf(RemoteModule.module, HomeModule.module)
}