package br.com.luis.gistlist.feature.home.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import br.com.luis.gistlist.R
import br.com.luis.gistlist.databinding.GistListItemBinding
import br.com.luis.gistlist.feature.home.domain.model.GistResponseModel
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.gist_list_item.view.*

class GistListAdapter(
    private val lifecycleOwner: Lifecycle,
    private val onItemClick: (GistResponseModel) -> Unit,
    private val onFavoriteClick: () -> Unit
) : ListAdapter<GistResponseModel, GistListAdapter.GistViewHolder>(object :
    DiffUtil.ItemCallback<GistResponseModel>() {
    override fun areItemsTheSame(oldItem: GistResponseModel, newItem: GistResponseModel): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(
        oldItem: GistResponseModel,
        newItem: GistResponseModel
    ): Boolean {
        return oldItem.owner == newItem.owner &&
                oldItem.description == newItem.description &&
                oldItem.type == newItem.type
    }

}) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GistViewHolder {
        val itemBinding =
            GistListItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return GistViewHolder(itemBinding)
    }

    override fun onBindViewHolder(holder: GistViewHolder, position: Int) {
        holder.bind(
            response = getItem(position),
            lifecycleOwner = lifecycleOwner,
            itemClickListener = onItemClick,
            favoriteClickListener = onFavoriteClick
        )
    }

    inner class GistViewHolder(item: GistListItemBinding) : RecyclerView.ViewHolder(item.root),
        LifecycleObserver {
        fun bind(
            response: GistResponseModel,
            lifecycleOwner: Lifecycle,
            itemClickListener: (GistResponseModel) -> Unit,
            favoriteClickListener: () -> Unit
        ) {
            lifecycleOwner.addObserver(this)
            itemView.gistListItemName.text =
                itemView.context.getString(R.string.gist_list_name, response.owner.name)
            itemView.gistListItemType.text =
                itemView.context.getString(R.string.gist_list_type, response.type)
            Picasso.get()
                .load(response.owner.avatar)
                .error(R.drawable.ic_placeholder)
                .into(itemView.gistListItemPhoto)

            itemView.gistListItemFavorite.setOnClickListener {
                itemView.gistListItemFavorite.playAnimation()
                    favoriteClickListener.invoke()
            }
            itemView.gistListItemContainer.setOnClickListener {
                itemClickListener.invoke(response)
            }
        }

        @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
        private fun cancelRequest() {
            Picasso.get().cancelRequest(itemView.gistListItemPhoto)
        }
    }

}