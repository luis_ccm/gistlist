package br.com.luis.gistlist.feature.home.ui.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import br.com.luis.core.extensions.openUrl
import br.com.luis.core.extensions.viewBinding
import br.com.luis.gistlist.R
import br.com.luis.gistlist.databinding.FragmentGistDetailBinding
import br.com.luis.gistlist.feature.home.domain.model.GistResponseModel
import com.squareup.picasso.Picasso

class GistDetailFragment : Fragment() {

    private var binding by viewBinding<FragmentGistDetailBinding>()
    private val gistDetail: GistResponseModel by lazy {
        val args: GistDetailFragmentArgs by navArgs()
        args.gistDetail
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentGistDetailBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        showGistDetailData()
        setListeners()
    }

    private fun showGistDetailData() {
        binding.run {
            Picasso.get()
                .load(gistDetail.owner.avatar)
                .error(R.drawable.ic_placeholder)
                .into(gistDetailPhoto)

            gistDetailName.text = gistDetail.owner.name
            gistDetailItemType.text = gistDetail.type
            gistDetailDate.text = getString(R.string.title_created_at, gistDetail.createdAt)
            gistDetailDescription.text = gistDetail.description
            gistDetailUrl.text = getString(R.string.title_url, gistDetail.owner.url)
        }
    }

    private fun setListeners() {
        binding.gistDetailUrl.setOnClickListener {
            openUrl(gistDetail.owner.url)
        }
    }
}