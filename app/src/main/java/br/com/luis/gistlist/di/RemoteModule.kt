package br.com.luis.gistlist.di

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object RemoteModule {
    private const val url = "https://api.github.com/gists/"

    val module = module {
        single<Gson> {
            GsonBuilder().create()
        }

        single<Retrofit> {
            Retrofit.Builder()
                .baseUrl(url)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
        }
    }
}