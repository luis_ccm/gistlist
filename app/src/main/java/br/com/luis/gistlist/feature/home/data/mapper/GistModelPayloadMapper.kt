package br.com.luis.gistlist.feature.home.data.mapper

import br.com.luis.gistlist.feature.home.data.model.GistModelPayload
import br.com.luis.gistlist.feature.home.data.model.GistOwnerPayload
import br.com.luis.gistlist.feature.home.domain.model.GistOwnerResponseModel
import br.com.luis.gistlist.feature.home.domain.model.GistResponseModel


fun GistModelPayload.mapToDomain(): GistResponseModel {
    val typeKeyName = "type"
    val typeString = try {
        val typeMap: Map<String, Any> = this.files.entries.firstOrNull()?.value as Map<String, Any>
        typeMap[typeKeyName] as String?
    } catch (exception: Exception) {
        ""
    }
    return GistResponseModel(
        id = this.id,
        type = typeString.orEmpty(),
        description = this.description.orEmpty(),
        createdAt = this.createdAt.toString(),
        owner = this.owner.mapToDomain()
    )
}

fun GistOwnerPayload.mapToDomain(): GistOwnerResponseModel =
    GistOwnerResponseModel(
        id = this.id,
        name = this.login,
        avatar = this.avatarUrl,
        url = this.url
    )