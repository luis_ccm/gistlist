package br.com.luis.gistlist.feature.home.ui.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import br.com.luis.core.domain.GistError
import br.com.luis.core.extensions.onFailure
import br.com.luis.core.extensions.onSuccess
import br.com.luis.core.extensions.toLiveData
import br.com.luis.core.util.SingleLiveDataEvent
import br.com.luis.gistlist.feature.home.domain.model.GistResponseModel
import br.com.luis.gistlist.feature.home.domain.repository.GistsRepository
import kotlinx.coroutines.launch

class HomeViewModel(private val repository: GistsRepository) : ViewModel() {

    private val _gistListLiveData = SingleLiveDataEvent<List<GistResponseModel>>()
    val gistListLiveData = _gistListLiveData.toLiveData()
    private val _errorLiveData = SingleLiveDataEvent<GistError>()
    val errorLiveData = _errorLiveData.toLiveData()
    private val _loadingLiveData = SingleLiveDataEvent<Unit>()
    val loadingLiveData = _loadingLiveData.toLiveData()

    fun getGistList() {
        viewModelScope.launch {
            _loadingLiveData.postValue(Unit)
            repository.getPublicGists()
                .onSuccess {
                    _gistListLiveData.postValue(it)
                }.onFailure {
                    _errorLiveData.postValue(it)
                }
        }
    }
}