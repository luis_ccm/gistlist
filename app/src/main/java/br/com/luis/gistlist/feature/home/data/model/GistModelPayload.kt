package br.com.luis.gistlist.feature.home.data.model

import com.google.gson.annotations.SerializedName
import java.util.*

data class GistModelPayload(
    @SerializedName("id")
    val id: String,
    @SerializedName("files")
    val files: Map<String, Any>,
    @SerializedName("created_at")
    val createdAt: Date,
    @SerializedName("description")
    val description: String?,
    @SerializedName("owner")
    val owner: GistOwnerPayload
)
