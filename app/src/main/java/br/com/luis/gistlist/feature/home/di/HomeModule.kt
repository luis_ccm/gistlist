package br.com.luis.gistlist.feature.home.di

import br.com.luis.core.coroutine.CoroutineProvider
import br.com.luis.gistlist.feature.home.domain.repository.GistsRepositoryImpl
import br.com.luis.gistlist.feature.home.data.service.GistsService
import br.com.luis.gistlist.feature.home.domain.repository.GistsRepository
import br.com.luis.gistlist.feature.home.ui.viewmodel.HomeViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import retrofit2.Retrofit

object HomeModule {
    val module = module {
        single<GistsService> {
            get<Retrofit>().create(GistsService::class.java)
        }

        factory<CoroutineProvider> {
            CoroutineProvider()
        }

        single<GistsRepository> {
            GistsRepositoryImpl(service = get(), coroutineProvider = get())
        }

        viewModel<HomeViewModel> {
            HomeViewModel(repository = get())
        }
    }
}