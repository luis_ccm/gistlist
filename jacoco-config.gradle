/**
 * Apply it in the module build.script.
 */

apply plugin: 'jacoco'

jacoco {
    toolVersion = Versions.org_jacoco_core
    reportsDir = file("$buildDir/jacocoTestReport")
}

tasks.withType(Test) {
    jacoco.includeNoLocationClasses = true
    jacoco.excludes = ['jdk.internal.*'] // see related issue https://github.com/gradle/gradle/issues/5184#issuecomment-457865951
}

project.afterEvaluate {
    def flavors = android.productFlavors.collect { flavor -> flavor.name }

    if (!flavors) flavors.add('')

    def buildType = "Debug"
    flavors.each { variant ->
        def variantName
        if (!variant) {
            variantName = "${buildType}"
        } else {
            variantName = "${variant}${buildType}"
        }

        def unitTestTask = "test${variantName.capitalize()}UnitTest"
        def uiTestCoverageTask = "create${variantName.capitalize()}CoverageReport"

        def excludes = [
                '**/R.class',
                '**/R$*.class',
                '**/BuildConfig.*',
                '**/Manifest*.*',
                '**/*Test*.*',
                '**/com/example/databinding/*',
                '**/com/example/generated/callback/*',
                '**/android/databinding/*',
                '**/androidx/databinding/*',
                'androidx/**/*.*',
                '**/di/module/*',
                '**/*MapperImpl*.*',
                '**/*$ViewInjector*.*',
                '**/*$ViewBinder*.*',
                '**/BuildConfig.*',
                '**/*Component*.*',
                '**/*BR*.*',
                '**/Manifest*.*',
                '**/*$Lambda$*.*',
                '**/*Companion*.*',
                '**/*Module.*', /* filtering Dagger modules classes */
                '**/*Dagger*.*',/* filtering Dagger-generated classes */
                '**/*MembersInjector*.*',
                '**/*_Factory*.*',
                '**/*_Provide*Factory*.*',
                '**/*Extensions*.*',
                '**/*$Result.*', /* filtering `sealed` and `data` classes */
                '**/*$Result$*.*',/* filtering `sealed` and `data` classes */
                '**/*Args*.*', /* filtering Navigation Component generated classes */
                '**/*Directions*.*' /* filtering Navigation Component generated classes */
        ]

        def unitTestExcludes = [
                '**/R.class',
                '**/R$*.class',
                '**/BuildConfig.*',
                '**/Manifest*.*',
                '**/*Test*.*',
                '**/com/example/databinding/*',
                '**/com/example/generated/callback/*',
                '**/android/databinding/*',
                '**/androidx/databinding/*',
                'androidx/**/*.*',
                '**/*Activity.*',
                '**/*Fragment.*',
                '**/*Fragment*.*',
                '**/*Adapter.*',
                '**/*Adapter*.*',
                '**/di/module/*',
                '**/*Application*.*',
                '**/*MapperImpl*.*',
                '**/*$ViewInjector*.*',
                '**/*$ViewBinder*.*',
                '**/BuildConfig.*',
                '**/*Component*.*',
                '**/*BR*.*',
                '**/Manifest*.*',
                '**/*$Lambda$*.*',
                '**/*Companion*.*',
                '**/*Module.*', /* filtering Dagger modules classes */
                '**/*Dagger*.*',/* filtering Dagger-generated classes */
                '**/*MembersInjector*.*',
                '**/*_Factory*.*',
                '**/*_Provide*Factory*.*',
                '**/*Extensions*.*',
                '**/*$Result.*', /* filtering `sealed` and `data` classes */
                '**/*$Result$*.*',/* filtering `sealed` and `data` classes */
                '**/*Args*.*', /* filtering Navigation Component generated classes */
                '**/*Directions*.*' /* filtering Navigation Component generated classes */
        ]

        def unitDebugTree = fileTree(dir: "$buildDir/tmp/kotlin-classes/${variantName}", excludes: unitTestExcludes)
        def fullDebugTree = fileTree(dir: "$buildDir/tmp/kotlin-classes/${variantName}", excludes: excludes)
        def mainSrc = "$project.rootDir/app/src/main/java"

        task "unitTestReport${variantName.capitalize()}"(type: JacocoReport, dependsOn: ["$unitTestTask"]) {
            group = "Reporting"
            description = "Generate Jacoco coverage reports on the ${variantName.capitalize()} build."

            reports {
                html.enabled = true
                xml.enabled = true
                csv.enabled = false
            }

            classDirectories.setFrom(files([unitDebugTree]))
            additionalSourceDirs.setFrom(files(mainSrc))
            sourceDirectories.setFrom(files([mainSrc]))
            def unitTestingData = [fileTree(dir: project.buildDir, includes: ["jacoco/${unitTestTask}.exec"])]
            executionData.setFrom(files([unitTestingData]))
        }

        task "fullTestReport${variantName.capitalize()}"(type: JacocoReport, dependsOn: ["$unitTestTask", "$uiTestCoverageTask"]) {
            group = "Reporting"
            description = "Generate Jacoco coverage reports on the ${variantName.capitalize()} build."

            reports {
                html.enabled = true
                xml.enabled = true
                csv.enabled = false
            }

            classDirectories.setFrom(files([fullDebugTree]))
            additionalSourceDirs.setFrom(files(mainSrc))
            sourceDirectories.setFrom(files([mainSrc]))

            def testingData = [fileTree(dir: project.buildDir, includes: ["jacoco/${unitTestTask}.exec", "outputs/code_coverage/${variantName}AndroidTest/connected/*.ec"])]
            executionData.setFrom(files([testingData]))
        }

    }
}
