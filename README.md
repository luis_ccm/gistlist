I made this project based on the project with some modifications:
https://gitlab.com/luis_ccm/gist-challenge.git

I decided to focus this project on architectural features, code quality and test coverage.
So I simplified the app, having a list of gists and a details screen.
I used a few different approaches like:
I created a core module to simulate a multi repo project.
Implemented detekt and lint to simulate use in a pipeline, or can be configured in preBuild
I implemented jacoco as a test coverage analyzer:
There is a unitTestReportDebug task that generates a complete unit test coverage report
I created a fullTestReportDebug task that generates a full coverage report of unit + instrumented tests, but for some reason
the ui test coverage is at 0%, running a createDebugCoverageReport task returns the ui test coverage correctly.
The idea with unified coverage would be to later send this result to Sonar for example
