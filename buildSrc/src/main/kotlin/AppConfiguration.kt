object AppConfiguration {

    object Plugins {
        const val androidApplication = "com.android.application"
        const val kotlinAndroid = "android"
        const val kotlinAndroidExtensions = "android.extensions"
        const val kotlinAndroidPlugin = "kotlin-android"
        const val safeArgs = "androidx.navigation.safeargs.kotlin"
        const val androidLibrary = "com.android.library"
    }

    const val compileSdkVersion = 30
    const val buildToolsVersion = "30.0.3"
    const val applicationId = "br.com.luis.gistlist"
    const val minSdkVersion = 21
    const val targetSdkVersion = 30
    const val versionCode = 1
    const val versionName = "1.0"
    const val testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"

    sealed class BuildType(
        val name: String,
        val isMinifyEnabled: Boolean = false,
        val isShrinkResources: Boolean = false,
        val defaultProguardFile: String = "",
        val defaultProguardRules: String = "",
        val suffix: String = ""
    ) {
        object Release : BuildType(
            name = "release",
            isMinifyEnabled = true,
            isShrinkResources = true,
            defaultProguardFile = "proguard-android-optimize.txt",
            defaultProguardRules = "proguard-rules.pro"
        )

        object Debug : BuildType(
            name = "debug",
            suffix = ".debug"
        )
    }


    val libsImplementations = listOf(
        Libs.kotlin_stdlib,
        Libs.androidx_core_core_ktx,
        Libs.appcompat,
        Libs.material,
        Libs.navigation_fragment_ktx,
        Libs.navigation_ui_ktx,
        Libs.lifecycle_common_java8,
        Libs.lifecycle_extensions
    )

    val appImplementations = listOf(
        Libs.lottie,
        Libs.koin_android,
        Libs.koin_core,
        Libs.koin_androidx_viewmodel,
        Libs.gson,
        Libs.retrofit,
        Libs.com_squareup_retrofit2_converter_gson,
        Libs.picasso
    )

    val debugImplementations = listOf(
        Libs.fragment_testing,
        Libs.androidx_test_core_ktx,
        Libs.core_testing
    )
    val testImplementationLibs = listOf(
        Libs.junit_junit,
        Libs.koin_test,
        Libs.mockk,
        Libs.mockwebserver,
        Libs.kotlinx_coroutines_test,
        Libs.core_testing
    )

    val androidTestImplementationLibs = listOf(
        Libs.androidx_test_ext_junit,
        Libs.espresso_core,
        Libs.navigation_testing,
        Libs.kotlinx_coroutines_test,
        Libs.mockk_android,
        Libs.espresso_contrib,
        Libs.espresso_intents
    )
}